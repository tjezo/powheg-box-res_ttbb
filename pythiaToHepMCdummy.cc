#include "HepMC/GenEvent.h"

extern "C" {

	HepMC::GenEvent * HEPEVTtoHepMC();
	HepMC::GenEvent * PythiaToHepMC(){
		return HEPEVTtoHepMC();	
	};

}

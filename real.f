      subroutine setreal(p,rflav,amp2)
c Wrapper subroutine to call the OL real emission matrix
      use openloops_powheg, only: ol_real => openloops_real
      implicit none
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'nlegborn.h'
      real * 8, intent(in)  :: p(0:3,nlegreal)
      integer,  intent(in)  :: rflav(nlegreal)
      real * 8, intent(out) :: amp2

      integer i
      real * 8 dotp
      external dotp
      logical :: debug = .false.

      if(debug) then
      print*, "==="
      do i=1,nlegreal
        print*, p(:,i), dotp(p(:,i),p(:,i)), sqrt(dotp(p(:,i),p(:,i)))
      end do
      end if
cDBG      print*,"real momenta:", p
cDBG      print*,"real flav:", rflav
      call ol_real(p,rflav,amp2)
cDBG      print*,"real amplitude:", amp2
      end


      subroutine realcolour_lh
c Wrapper subroutine to call the MadGraph code to associate
c a (leading) color structure to an event.
      implicit none
      include 'nlegborn.h'
      include 'LesHouches.h'
      integer rflav(nlegreal),color(2,nlegreal)
      integer i,j
      end


      subroutine regularcolour_lh
c Wrapper subroutine to call the OL code to associate
c a (leading) color structure to an event.
      use openloops_powheg
      implicit none
      include 'nlegborn.h'
      include 'LesHouches.h'
      include 'pwhg_rad.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      real * 8 p(0:3,nlegreal)
      integer rflav(nlegreal),is_fs_flav(nlegreal),
     1     is_fs(nlegreal),isfslength,color(2,nlegreal)
      integer j

      do j=1,nlegreal
        rflav(j)=idup(j)
        if (rflav(j).eq.21) rflav(j)=0
        p(:,j) = kn_cmpreal(:,j)
      enddo

      ! call OL colorflow
      call openloops_realcolour(p,rflav,color)

      do j=1,nlegreal
        icolup(:,j)=color(:,j)
      enddo

      end




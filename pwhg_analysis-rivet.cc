#include "Rivet/AnalysisHandler.hh"
#include "HepMC/GenEvent.h"
#include "HepMC/IO_HEPEVT.h"
#include <sstream>
#include <assert.h>
#include <cstring>
#include "powheginput_cpp.h"

extern "C"{
  extern struct{
    int nup, idprup;
    double xwgtup, scalup, aqedup, aqcdup;
    int idup[500], istup[500], mothup[1000], icolup[1000];
    double pup[2500], vtimup[500], spinup[500];
  } hepeup_;
}

using namespace std;

// Create analysis handler
Rivet::AnalysisHandler rivet;

// count number of events analyzed
int eventCounter = 0;
int entryCounter = 0;
double currentContrXsec = 0.0;
double currentContribEventCounter = 0.0;
double totalXsec = 0.0;
int rivetWeight = 0;
const int maxWeights = 200;
int noe = -1;

HepMC::IO_HEPEVT hepevtio;
HepMC::IO_GenEvent * ascii_out;

// HepMC output switches
bool st2HepMC = false, st4HepMC = false, st6HepMC = false;
// stage information
int pstage, xgit;

extern "C" {

// fortran common block declarations
  struct PWHG_RND{
    int rnd_numseeds,rnd_initialseed,rnd_iwhichseed;
    char rnd_cwhichseed[4];
    int rnd_i1,rnd_i2;
  } pwhg_rnd_;

  struct RADTYPE{
    int radtype;
  } radtype_;

  // fortran functions declarations
  void multi_plot_setup_(double&, double*, const int&);
  void getparallelparms_(int&, int&);

// extern C function declaration from pythia82F77.cc
  HepMC::GenEvent * PythiaToHepMC();

// fotran functions implementations
  void setupmulti_(int &n){};

  void pwhgtopout_(char* filename, int len){};

  void pwhgsetout_(){};

  void pwhgaddout_(){
    if (currentContribEventCounter > 0) {
      std::cout << "[Rivet analysis] Adding xsec contribution: " << currentContrXsec << ", " << currentContribEventCounter << std::endl;
      if (noe < 0)
        totalXsec += currentContrXsec/currentContribEventCounter;
      else 
        totalXsec += currentContrXsec/noe;
      currentContrXsec = currentContribEventCounter = 0;
    }
  };
  // allows user to set the number of events, if set to -1 the events will be counted
  void pwhgsetnumberofevents_(int & numberOfEvents){
    noe = numberOfEvents;
  }

  void pwhgaccumup_(){ eventCounter++; currentContribEventCounter++; };

  void init_hist_(){
    if (powheginput("#rivetTraceLogLevel") == 1) Rivet::Log::setLevel("Rivet", Rivet::Log::TRACE);
    string option = "#rivetAnalysis";
    string analysis;
    powheginputstring(option, analysis);
    //analysis = "hxswg_ttbjets_part_v1bNLO_decay"; 
    if ( !analysis.empty() ) {
      rivet.addAnalysis(analysis);
      int i = 0;
      while (true) {
        powheginputstring(option+to_string(++i), analysis);
        if ( analysis.empty() ) break;
        rivet.addAnalysis(analysis);
      }
    }
    // set hepevtio settings
    hepevtio.set_trust_mothers_before_daughters(true);
    hepevtio.set_trust_beam_particles(false);
    // get parallel stage info
    getparallelparms_(pstage, xgit);
    // get rivet powheg.input settings
    if (powheginput("#testplotsHepMC") == 1 && pstage == 2) st2HepMC = true;
    if (powheginput("#HepMCEventOutput") == 1 && pstage == 4) st4HepMC = true;
    if (powheginput("#HepMCEventOutput") == 1 && pstage == 6) st6HepMC = true;
    if (st2HepMC || st4HepMC || st6HepMC) {
      // open the disk output
      string swhichseed(pwhg_rnd_.rnd_cwhichseed);
      if (st2HepMC) ascii_out = new HepMC::IO_GenEvent("pwgevents-NLO-"+swhichseed+".HepMC",std::ios::out);
      else if (st4HepMC) ascii_out = new HepMC::IO_GenEvent("pwgevents-"+swhichseed+".HepMC",std::ios::out);
      else if (st6HepMC) ascii_out = new HepMC::IO_GenEvent("pwgevents-NLOPS-"+swhichseed+".HepMC",std::ios::out);
    }
    rivetWeight = powheginput("#rivetWeight");
    if (rivetWeight < 0) rivetWeight = 0;
  }

  HepMC::GenEvent * HEPEVTtoHepMC(){
    // get the event from the hepevt common block and convert it into HepMC
    HepMC::GenEvent* evt = hepevtio.read_next_event();
  
    // AND the following doesn't work for some obscure reason
//    for (HepMC::GenEvent::particle_const_iterator p=evt->particles_begin(); p!=evt->particles_end(); p++) 
//    {
//      std::cout << (*p)->barcode() << ", " << std::endl;
//      (*p)->suggest_barcode((*p)->barcode()+10000);
//    }
    // SO we copy the whole event into a new event
    HepMC::GenEvent* fixed_evt = new HepMC::GenEvent();
    // loop over all the vertices
    for (HepMC::GenEvent::vertex_const_iterator v=evt->vertices_begin(); v!=evt->vertices_end(); v++){
      HepMC::GenVertex* new_vertex = new HepMC::GenVertex();
      for (HepMC::GenVertex::particles_in_const_iterator p=(*v)->particles_in_const_begin(); p!=(*v)->particles_in_const_end(); p++) {
        HepMC::GenParticle* new_particle = new HepMC::GenParticle(**p);
        new_particle->suggest_barcode(new_particle->barcode()+10000);
        if (new_particle->pdg_id() == 0) new_particle->set_pdg_id(21);
        new_vertex->add_particle_in(new_particle);
      }
      for (HepMC::GenVertex::particles_out_const_iterator p=(*v)->particles_out_const_begin(); p!=(*v)->particles_out_const_end(); p++) {
        HepMC::GenParticle* new_particle = new HepMC::GenParticle(**p);
        new_particle->suggest_barcode(new_particle->barcode()+10000);
        if (new_particle->pdg_id() == 0) new_particle->set_pdg_id(21);
        new_vertex->add_particle_out(new_particle);
      }
      fixed_evt->add_vertex(new_vertex);
    }
    fixed_evt->set_event_number(eventCounter);
    delete evt;
    return fixed_evt;
  }

  void analysis_(double& dsig0){

    //get all the weights from POWHEG
    double dsig[maxWeights];
    multi_plot_setup_(dsig0,dsig,maxWeights);
    assert(dsig0 == dsig[0]);

    // add weight to the xsection
    currentContrXsec += dsig[rivetWeight];

    // do the conversion to HepMC
    HepMC::GenEvent * evt;

    if (powheginput("#pythiaHepMC") == 1) {
      evt = PythiaToHepMC();
    }
    else {
      evt = HEPEVTtoHepMC();
    }

    //// make sure all the weight are copied over properly
    // 1.) destroy the existing weights to unify the treatment (Pythia HepMC will have 1 weight, HEPEVT 0 weights)
    evt->weights().clear();
    // 3.) transfer them into HepMC
    // rivet will probably use only one weight, so we let user to choose which weight to use by default
    // copy over the rivetWeight weight (even if it is zero -- otherwise Rivet counts them as unit weight events)
    evt->weights().push_back(dsig[rivetWeight]);
    // copy also all the other non-zero events
    for (int i=0; i<maxWeights; i++) {
      if (i!=rivetWeight and dsig[i] != 0.0) {
        evt->weights().push_back(dsig[i]);
      }
    }

    // append 1 to the event record if the event type is btl, 2 if the event type is rmn
    if (radtype_.radtype > 0 && radtype_.radtype < 3) {
      evt->weights().push_back(radtype_.radtype);
    }
    else { };

    // copy over the scalup
    evt->set_event_scale(hepeup_.scalup); 

    // output the event
    if (st2HepMC || st4HepMC || st6HepMC) {
      (*ascii_out) << evt;
    }

    // analyze it with rivet
    rivet.analyze(*evt);

    // delete the event
    delete evt;
  }

  void finalize_(char* cwichseed, int lencwichseed){
    // call pwhgaddout to make sure the cross section counter is added to the totalXsec at least once
    // no problem if it has been called just before, because the temp counters are reset in this call
    pwhgaddout_();
    
    string swhichseed(pwhg_rnd_.rnd_cwhichseed);
    if (st2HepMC || st4HepMC || st6HepMC) {
      delete ascii_out;
    }
    rivet.setCrossSection(totalXsec);
    std::cout << "[Rivet analysis] The total cross section set to: " << totalXsec << std::endl;
    rivet.finalize();
    std::stringstream yoda;
    yoda << "pwgevents-st" << pstage << "-" << swhichseed << "-W" << rivetWeight << ".yoda";
    rivet.writeData(yoda.str());
  }
}
